


## Terminology

"data-point" is a single record from the Crossref database.
"sample" is a collection of datapoints taken at a particular time with a set of particular contraints (e.g. date, member_id).




The general challenge of this is mapping s3 object keys to reasonable REST-ish end points.

More specifically, it is a PITA to provide the user with a folow-your-nose list of samples and data-points that are available.

Looking at the bucket `samples.research.crossref.org`, we have

"all-member" samples and datapoints- that is s


## all-works

file: `all-works/context.jsonl`

Example:

```
{"name": "all works sample", "api-request": "works", "s3-prefix": "works", "sample-size": 5000}
```

Questions- this lives at the top of the `all-works` hierarchy, but I'm not sure what it is supposed to capture. For example, the implication is that the `sample-size` would apply to each "per-date-sample" underneath the hierarchy, but preumably, as the entire corpus grows, there will be a time when the sample-size will have to grow and so there will be some dates where the advertised sample size is correct and others where it is not.

So how do the contents of the `all-works/context.jsonl` file relate to the "per-date-sample" `sample-metdata.json` file which contains something like this:

```
{"total-data-points": 5000, "collected-date": "2022-09-20"}
```

This latter file feels more useful.

Why are the datapoints in the "per-date-sample" contained in individual `json` files for each datapoint instead of one `jsonl` file containing all the data-points? It feels like a single `json` or `jsonl` file would make the whole thing more tractable.

Related to the above- why the "works" subdirectory under each "per-date-sample"? This is, after-all, a subdirectory fo the "all-works" subdirectory, I would assume the data-points underneath would be "works" just from that.

Why are we keeping the sample metadata outside of the sample file itself? I feel like we should be aiming to:

keep the representation of the datapoint files as close to the REST API representation as possible. This will make it easier for people to reuse code that they've developed for the API.

And we can always fold the sample metadata into a non-conclicting key included in the datapoint file itslef. 
