import json
import logging
import shutil
from datetime import date, timedelta

import boto3
import botocore
import pandas as pd
import typer
from botocore import UNSIGNED
from botocore.config import Config
from diskcache import FanoutCache
from rich.console import Console
from rich.logging import RichHandler

# CACHE_TIMEOUT = 60 * 60 * 24
CACHE_TIMEOUT = None


class S3ObjException(Exception):
    pass


logging.basicConfig(level="INFO", handlers=[RichHandler(console=Console(stderr=True))])
logger = logging.getLogger(__name__)

BUCKET = "samples.research.crossref.org"
MAX_RETROSPECTIVE_DAYS = 14

session = boto3.Session()
config = Config(signature_version=UNSIGNED)

s3 = session.resource("s3", config=config)
my_bucket = s3.Bucket(BUCKET)

cache = FanoutCache(
    eviction_policy="least-frequently-used",
    size_limit=int(5e9),
    shards=8,
    directory="s3_index_cache",
)
logger.info(f"Cache size: {cache.size_limit}")


def disk_info():
    total, used, free = shutil.disk_usage("/")

    return {
        "total": (total // (2**30)),
        "used": (used // (2**30)),
        "free": (free // (2**30)),
    }


PER_MEMBER_METADATA_S3_PATH_TEMPLATE = (
    "members-works/sample-{iso_date}/works/member-{member_id}.jsonl"
)


def s3_key_exists(s3_path):
    """
    If the object exists, return True, otherwise return False

    :param s3_path: The path to the object in S3
    :return: A boolean value.
    """
    logger.info(f"Checking for existence of: {s3_path}")
    try:
        my_bucket.Object(s3_path).load()
        return True
    except botocore.exceptions.ClientError as e:
        if e.response["Error"]["Code"] == "404":
            return False
        logger.error(f"Error getting s3 object: {s3_path}")
        logger.error(e)
        raise S3ObjException(e) from e


def current_allowable_date_range():
    """
    Return a list of dates between yesterday and MAX_RETROSPECTIVE_DAYS days ago
    :return: A list of dates between the start and end dates.
    """
    end = date.today()
    start = end - timedelta(days=MAX_RETROSPECTIVE_DAYS)
    return pd.date_range(start, end - timedelta(days=1), freq="d")


def s3_obj_to_str(s3_path):
    logger.info(f"Getting s3 object: {s3_path}")
    try:
        return my_bucket.Object(s3_path).get()["Body"].read().decode("utf-8")
    except Exception as e:
        logger.error(f"Error getting s3 object: {s3_path}")
        logger.error(e)
        raise S3ObjException(e) from e

@cache.memoize(expire=CACHE_TIMEOUT)
def cache_s3_obj_to_str(s3_path):
    logger.info(f"Caching s3 object: {s3_path}")
    return s3_obj_to_str(s3_path)


def latest_current_allowable_per_member_date(member_id):
    """
    Return the latest date for which there is a per-member works data point file in S3

    :param member_id: The member id of the member you want to get the latest date for
    :return: The latest date for which there is a per-member data point.
    """
    return next(
        (
            test_date.date()
            for test_date in reversed(current_allowable_date_range())
            if s3_key_exists(
                per_member_works_data_points_s3_path(member_id, test_date.date())
            )
        ),
        None,
    )


def per_member_works_data_points_s3_path(member_id, iso_date):
    """
    Take a member ID and an ISO date, and return the path to the S3 object that contains the works
    data for that member on that date

    :param member_id: the id of the member
    :param iso_date: the date of the sample, in ISO format (YYYY-MM-DD)
    :return: A list of dictionaries.
    """
    return f"members-works/sample-{iso_date}/works/member-{member_id}.jsonl"


def fold_data_point_into_item(jsonl_line):
    """
    Hoist the data-point-id key into the item itself so that the structure of the response is the same
    as the structure of the response from the REST API works endpoint.

    :param jsonl_line: a line of JSONL
    :return: A dictionary with the data-point and data-point-id
    """
    j = json.loads(jsonl_line)
    return j["data-point"] | {"data-point-id": j["data-point-id"]}


def per_member_works_data_points(member_id, iso_date):
    return [
        fold_data_point_into_item(datapoint)
        for datapoint in s3_obj_to_str(
            per_member_works_data_points_s3_path(member_id, iso_date)
        ).split("\n")
    ]


@cache.memoize(expire=CACHE_TIMEOUT)
def per_member_data_points_exist(member_id, iso_date):
    return s3_key_exists(per_member_works_data_points_s3_path(member_id, iso_date))


###############################


def all_member_works_metadata_s3_path(iso_date):
    return f"all-works/sample-{iso_date}/sample-metadata.json"


@cache.memoize(name="all_member_works_data_points", expire=CACHE_TIMEOUT)
def all_member_works_data_points(iso_date):
    logger.warning("This should be cached")
    return [
        json.loads(cache_s3_obj_to_str(object.key))
        for object in my_bucket.objects.filter(
            Prefix=f"all-works/sample-{iso_date}/works"
        )
    ]


def latest_current_allowable_all_member_date():
    """
    Returns the latest date for which there is a file in S3 containing the metadata for all member
    works
    :return: The latest date for which there is a file in the S3 bucket.
    """
    return next(
        (
            test_date.date()
            for test_date in reversed(current_allowable_date_range())
            if s3_key_exists(all_member_works_metadata_s3_path(test_date.date()))
        ),
        None,
    )


@cache.memoize(expire=CACHE_TIMEOUT)
def all_member_data_points_exist(iso_date):
    return s3_key_exists(all_member_works_metadata_s3_path(iso_date))


def prime_cache():
    logger.info("Priming cache")
    for iso_date in current_allowable_date_range():
        all_member_works_data_points(iso_date.date())


def main():
    prime_cache()
    print("Done")


if __name__ == "__main__":
    typer.run(main)
