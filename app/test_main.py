from fastapi.testclient import TestClient

from .main import app

client = TestClient(app)


def test_read_main():
    response = client.get("/")
    assert response.status_code == 200
    assert response.json() == {"message": "Synclavier: A sample data API from Crossref Labs"}

def test_read_all_member_samples():
    response = client.get("/members/samples")
    assert response.status_code == 200
    assert "status" in response.json() 

def test_get_latest_per_member_sample():
    response = client.get("/members/98/samples/latest")
    assert response.status_code == 200
   

