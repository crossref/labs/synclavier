import logging
import time
from datetime import date

import sentry_sdk
from aioprometheus import Counter, Gauge, MetricsMiddleware
from aioprometheus.asgi.starlette import metrics
from fastapi import FastAPI, Path, Request, status
from fastapi.responses import ORJSONResponse, RedirectResponse
from fastapi.staticfiles import StaticFiles
from fastapi_utils.session import FastAPISessionMaker
from fastapi_utils.tasks import repeat_every

from app.crutils import (all_member_data_points_exist,
                         all_member_works_data_points,
                         current_allowable_date_range, disk_info,
                         latest_current_allowable_all_member_date,
                         latest_current_allowable_per_member_date,
                         per_member_data_points_exist,
                         per_member_works_data_points, prime_cache)

APP_NAME = "Synclavier"
ABOUT = "A sample data API from Crossref Labs"
REPRESENTAION_VERSION = "1.0.0"
CONTACT = {
    "name": "Crossref Labs",
    "url": "http://crossref.org/labs",
    "email": "labs@crossref.org",
}
LICENSE_INFO = {
    "name": "Apache 2.0",
    "url": "https://www.apache.org/licenses/LICENSE-2.0.html",
}

MEMBER_ID_TILE = "The member ID"
MEMBER_ID_DESCRIPTION = "The member ID as obtained from the REST API"

ISO_DATE_TITLE = "The date of the sample"
ISO_DATE_DESCRIPTION = "The date of the sample in ISO format (YYYY-MM-DD)"

# Start warning in heartbeat if diskspace falls below the following
MIN_DISK_SPACE = 1  # 1GB


class ExampleSentryException(Exception):
    pass


# NB you need to set the SENTRY_DSN environment variable
sentry_sdk.init(
    # Set traces_sample_rate to 1.0 to capture 100%
    # of transactions for performance monitoring.
    # We recommend adjusting this value in production,
    traces_sample_rate=1.0,
)


logging.basicConfig(level="INFO")
logger = logging.getLogger(__name__)


with open("README.md", "r") as f:
    ABOUT = f.read()

app = FastAPI(
    description=ABOUT,
    title="Synclavier",
    version="0.1.0",
    contact=CONTACT,
    license_info=LICENSE_INFO,
)
app.mount("/static", StaticFiles(directory="static"), name="static")

database_uri = "sqlite:///./test.db?check_same_thread=False"
sessionmaker = FastAPISessionMaker(database_uri)

app.state.all_member_counter = Counter(
    "all_member_total", "Number of all member sample requests"
)
app.state.all_member_latest_counter = Counter(
    "all_member_latest_total", "Number of all member sample requests"
)

app.state.per_member_counter = Counter(
    "per_member_total", "Number of per-member sample requests"
)
app.state.per_member_latest_counter = Counter(
    "per_member_latest_total", "Number of per-member sample requests"
)

app.state.disk_metric = Gauge("disk_usage_gb", "Disk usage in GB")


app.add_middleware(MetricsMiddleware)
app.add_route("/metrics", metrics)


@app.on_event("startup")
@repeat_every(seconds=60 * 60)  # 1 hour
def prime_cache_task() -> None:
    with sessionmaker.context_session() as db:
        logger.info("Priming cache")
        prime_cache()
        logger.info("Finished priming cache")


def response_header(status, message_type):
    return {
        "status": status,
        "message-type": message_type,
        "message-version": REPRESENTAION_VERSION,
    }


def response_message(message):
    return {"message": message}


@app.get("/")
async def root():
    """This API Documentation"""
    return RedirectResponse(
        "/docs#",
        status_code=status.HTTP_302_FOUND
    )


##### All member samples


@app.get("/members/samples")
async def get_all_member_sample_available(request: Request):
    """a JSON list of all the all-member samples available"""
    host = request.headers.get("x-forwarded-host", request.headers.get("host"))
    path = app.url_path_for(
        "get_all_member_data_points_for_date", **{"iso_date": "{iso_date}"}
    )
    url_template = f"https://{host}{path}"

    items = [
        url_template.format(iso_date=iso_date.date())
        for iso_date in current_allowable_date_range()
        if all_member_data_points_exist(iso_date.date())
    ]
    header = response_header("ok", "sample-uri-list")
    message = response_message({"sample-count": len(items), "items": items})

    return ORJSONResponse(
        content=header | message,
        status_code=status.HTTP_200_OK,
        media_type="application/json",
    )


@app.get("/members/samples/latest")
async def get_latest_all_member_sample():
    app.state.all_member_latest_counter.inc({})
    # Just redirect to the latest all-member sample
    return RedirectResponse(
        f"/members/samples/{latest_current_allowable_all_member_date()}",
        status_code=status.HTTP_302_FOUND,
    )


@app.get("/members/samples/{iso_date}")
async def get_all_member_data_points_for_date(
    iso_date: date = Path(
        ...,
        title=ISO_DATE_TITLE,
        description=ISO_DATE_DESCRIPTION,
    )
):
    app.state.all_member_counter.inc({"date": iso_date.isoformat()})
    try:
        logger.info(f"Getting all-member data points for {iso_date}")
        items = all_member_works_data_points(iso_date)
        header = response_header("ok", "data-points-list")
        message = response_message({"data-points-count": len(items), "items": items})

        return ORJSONResponse(
            content=header | message,
            status_code=status.HTTP_200_OK,
        )
    except Exception as e:
        return ORJSONResponse(
            content={"error": str(e)},
            status_code=status.HTTP_404_NOT_FOUND,
            media_type="application/json",
        )


##### Per member samples


@app.get("/members/{member_id}/samples")
async def get_per_member_samples_available(
    request: Request,
    member_id: str = Path(
        ...,
        title=MEMBER_ID_TILE,
        description=MEMBER_ID_DESCRIPTION,
    ),
):
    host = request.headers.get("x-forwarded-host", request.headers.get("host"))
    path = app.url_path_for(
        "get_per_member_data_point_for_date",
        **{"member_id": "{member_id}", "iso_date": "{iso_date}"},
    )
    url_template = f"https://{host}{path}"

    # url_template = request.url_for(
    #     "get_per_member_data_point_for_date",
    #     **{"member_id": "{member_id}", "iso_date": "{iso_date}"},
    # )

    items = [
        url_template.format(member_id=member_id, iso_date=iso_date.date())
        for iso_date in current_allowable_date_range()
        if per_member_data_points_exist(member_id, iso_date.date())
    ]

    header = response_header("ok", "sample-uri-list")
    message = response_message({"sample-count": len(items), "items": items})

    return ORJSONResponse(
        content=header | message,
        status_code=status.HTTP_200_OK,
        media_type="application/json",
    )


@app.get("/members/{member_id}/samples/latest")
async def get_latest_per_member_sample(
    member_id: str = Path(
        ...,
        title=MEMBER_ID_TILE,
        description=MEMBER_ID_DESCRIPTION,
    )
):
    app.state.per_member_latest_counter.inc({})

    # Just redirect to the latest per-member sample
    if latest := latest_current_allowable_per_member_date(member_id):
        return RedirectResponse(
            f"/members/{member_id}/samples/{latest_current_allowable_per_member_date(member_id)}",
            status_code=status.HTTP_302_FOUND,
        )
    else:
        return ORJSONResponse(
            content={"error": f"No sample data for {member_id}"},
            status_code=status.HTTP_404_NOT_FOUND,
            media_type="application/json",
        )


@app.get("/members/{member_id}/samples/{iso_date}")
async def get_per_member_data_point_for_date(
    member_id: str = Path(title=MEMBER_ID_TILE, description=MEMBER_ID_DESCRIPTION),
    iso_date: date = Path(
        ...,
        title=ISO_DATE_TITLE,
        description=ISO_DATE_DESCRIPTION,
    ),
):
    app.state.per_member_counter.inc(
        {"date": iso_date.isoformat(), "member_id": member_id}
    )
    try:
        items = per_member_works_data_points(member_id, iso_date)
        header = response_header("ok", "data-points-list")
        message = response_message({"data-points-count": len(items), "items": items})
        return ORJSONResponse(
            content=header | message,
            status_code=status.HTTP_200_OK,
        )
    except Exception as e:
        return ORJSONResponse(
            content={"error": str(e)},
            status_code=status.HTTP_404_NOT_FOUND,
            media_type="application/json",
        )


@app.get("/heartbeat")
async def heartbeat():
    di = disk_info()
    logger.info(di)
    app.state.disk_metric.set({"type": "total"}, di["total"])
    app.state.disk_metric.set({"type": "used"}, di["used"])
    app.state.disk_metric.set({"type": "free"}, di["free"])

    status = "ok" if int(di["free"]) > MIN_DISK_SPACE else "warning"
    return {"status": f"{status}", "disk": di}


@app.get("/sentry-debug")
async def trigger_error():
    raise ExampleSentryException(
        "This is an example of an exception being sent to Sentry."
    )
