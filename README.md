
![image](static/cr-lab-creature3.png)

## Warnings, Caveats and Weasel Words

This is a Crossref Labs project. It has sharp edges and is likely to cut you. 🔪

## What?

An example API to access pre-generated samples of Crossref metadata.

It is named after [a famous polyphonic digital sampling system](https://en.wikipedia.org/wiki/Synclavier).

Ideally, this functionality will be integrated into Crossref's [REST API](https://api.crossref.org).

But implimenting this as a labs project first allows us to test and refine the concept before we do so.

## Why?

Representative samples of Crossref metadata are useful.

At the very least they are useful for helping you build meaningful tests. They can help you ensure that you are able to handle the wide variety of - **cough** - eccentricities that you are likely to encounter in Crossref metadata.

But they are also useful for doing research using Crossref metadata- particularly when you don't want to have to process hundreds of millions of records.

The [Crossref API](https://api.crossref.org) already has a `sample` paramter on some of its end-points. However, to use it for anything significant, you need to iterrate over cursors and this kind of sampling can be difficult and take a long time. Particularly- if you would like to conduct any longitudinal research using samples. We know this because we've had to do it.

So this API provides access to a set pre-generated samples that are created on a regular schedule.

There are two broad kinds of samples available here:

- all-works. These are samples accross all of our work types and members. They are useful if you want to geta quick picture of the overall state of Crossref metdata.

- per-member-works. These sample are taken on a (you guessed it) per-member basis. They are useful if you want to understand variation in practice across our memebrship. In other words- if you want to point fingers.

## How

We run (a sampling framework)[https://gitlab.com/crossref/sampling-framework] that routinely creates samples and stashes them in an s3 bucket.

This is simply a [FastAPI]() front-end that allows you to trieve said samples in a way that is consistent with Crossref's [REST API](https://api.crossref.org)

Currently there is a lot of impedance between a REST-ish way to do things and the way things are laid-out in the s3 bucket. Most of the work this API is doing is managing that imedance.

This empedance may be necessary. Fro example, it may be more useful to have the data decomposed like this if you are using highly parallel tools like SPARK to process the data. But the layout makes it much harder if you are using exisitng REST API tools.

For example, in the rest API you access information about a particular member like this:

`/members/{member_id}/`

So you would expect to be able to retrieve a member's samples like this:

`/members/{member_id}/samples`

And a member's sample on a particular date like this:

`/members/{member_id}/samples/{iso_date}`

But the s3 bucket is laid out like this:

`/members-works/sample-{iso_date}/works/member_{member_id}.jsonl`

Which means the FastAPI front-end has to do a lot of jiggery-pokery to translate the structures back and forth.

This jiggery-pokery involves caching the results of the translations because they are expensive to impliment on a per-request basis.

Similarly, if you were used to using the REST API to process works data and you might want the sample to come back in roughly the same representation as the REST API's:

```json
{
    "status": "ok",
    "message-type": "work-list",
    "message-version": "1.0.0",
    "message": {
        
        "items": [...], 
      
    }
    
}
```

So that you can reuse your code, however, as you se above the "items" in the s3 bucket are instead stored as `jsonl` to make them easier to process using SPARK.

So, again, this front-end trnaslates the `jsonl` back into a representation that is backward-compatible with the current REST API's representation.



## Where
- [Try it](https://samples.labs.crossref.org/members/sammples)
- [Documentation](https://samples.labs.crossref.org/docs#)
- [Source Code](https://gitlab.com/crossref/labs/synclavier)